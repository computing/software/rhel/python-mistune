%global upname mistune
%global with_python3 1

Name:           python-mistune
Version:        0.8.3
Release:        1.2%{?dist}
Summary:        Markdown parser for Python 

License:        BSD
URL:            https://github.com/lepture/mistune
Source0:        https://github.com/lepture/mistune/archive/v%{version}.tar.gz

BuildRequires:  python%{python3_pkgversion}-Cython
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-nose
BuildRequires:  python%{python3_pkgversion}-setuptools

%description
The fastest markdown parser in pure Python, inspired by marked.


%package -n python%{python3_pkgversion}-%{upname}
Summary:        Markdown parser for Python

%description -n python%{python3_pkgversion}-%{upname}
The fastest markdown parser in pure Python, inspired by marked.

%prep
%setup -q -n %{upname}-%{version}
# Moved to source archive from github, doesn't contain egg-info
#rm -rf mistune.egg-info

find . -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python3}|'


%build
CFLAGS="$RPM_OPT_FLAGS" %{__python3} setup.py build


%install
%{__python3} setup.py install --skip-build --root %{buildroot}

%{_fixperms} %{buildroot}/*

%check
%{__python3} setup.py test

%files -n python%{python3_pkgversion}-%{upname}
%doc LICENSE README.rst
%{python3_sitearch}/%{upname}.*
%{python3_sitearch}/%{upname}-%{version}-py?.?.egg-info
%{python3_sitearch}/__pycache__/%{upname}*


%changelog
* Thu Oct 24 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 0.8.3-1.2
- Drop support for python 3.4, python2

* Fri Nov 30 2018 Michael Thomas <michael.thomas@LIGO.ORG> - 0.8.3-1.1
- Update to support python 3.6

* Tue Dec 19 2017 Christian Dersch <lupinix@mailbox.org> - 0.8.3-1
- new version
- fixes CVE-2017-15612 and CVE-2017-16876

* Wed Jun 17 2015 Christian Dersch <lupinix@fedoraproject.org> - 0.6-1
- new upstream release

* Mon Apr 20 2015 Christian Dersch <lupinix@fedoraproject.org> - 0.5.1-1
- new upstream release (0.5.1)

* Tue Mar  3 2015 Christian Dersch <lupinix@fedoraproject.org> - 0.5-1
- initial package on epel7 
